<?php
/**
 * File name: ProductController.php
 * Last modified: 2020.04.29 at 18:37:35
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers;

use App\Criteria\Products\ProductsOfUserCriteria;
use App\DataTables\ProductDataTable;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\ProductOptions;
use App\Repositories\BrandRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CustomFieldRepository;
use App\Repositories\ProductOptionRepository;
use App\Repositories\StoreRepository;
use App\Repositories\ProductRepository;
use App\Repositories\UploadRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;
use Image;

class ProductController extends Controller
{
    /** @var  ProductRepository */
    private $productRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    /**
     * @var UploadRepository
     */
    private $uploadRepository;
    /**
     * @var StoreRepository
     */
    private $storeRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var BrandRepository
     */
    private $brandRepository;

    private $productOptionRepository;

    public function __construct(ProductRepository $productRepo, CustomFieldRepository $customFieldRepo, UploadRepository $uploadRepo
        , StoreRepository $storeRepo
        , CategoryRepository $categoryRepo
        ,BrandRepository $brandRepo
        ,ProductOptionRepository $productOptionRepository
    )
    {
        parent::__construct();
        $this->productRepository = $productRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->uploadRepository = $uploadRepo;
        $this->storeRepository = $storeRepo;
        $this->categoryRepository = $categoryRepo;
        $this->brandRepository =$brandRepo;
        $this->productOptionRepository =$productOptionRepository;
    }

    /**
     * Display a listing of the Product.
     *
     * @param ProductDataTable $productDataTable
     * @return Response
     */
    public function index(ProductDataTable $productDataTable)
    {
        return $productDataTable->render('products.index');
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {

        $category = $this->categoryRepository->pluck('name', 'id');
        $brand = $this->brandRepository->pluck('name', 'id');
        if (auth()->user()->hasRole('admin')) {
            $store = $this->storeRepository->pluck('name', 'id');
        } else {
            $store = $this->storeRepository->myActiveStores()->pluck('name', 'id');
        }
        $hasCustomField = in_array($this->productRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->productRepository->model());
            $html = generateCustomField($customFields);
        }
        return view('products.create')->with("customFields", isset($html) ? $html : false)->with("store", $store)->with("category", $category)->with("brand",$brand);
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $input = $request->all();
        $variataitonOptions = $request->all();
        unset($input['productOptions']);
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->productRepository->model());
        try {
            $product = $this->productRepository->create($input);
            if ($product){
                //Save  Product Options
                foreach ($variataitonOptions['productOptions'] as $key => $value) {

                    $productImageVariations = $request->file('productOptions');
                    $productImage=!empty($productImageVariations[$key]['image'])?$productImageVariations[$key]['image']:'';
                    $imageUrl='';
                    if ($productImage) {
                        $imageName = $productImage->getClientOriginalName();
                        $directory = 'productImage/';
                        $imageUrl = $directory . $imageName;
                        Image::make($productImage)->save($imageUrl);
                    }

                    $productOptions = new ProductOptions();
                    $productOptions->product_id =$product->id;
                    $productOptions->size = !empty($value['size'])?$value['size']:'';
                    $productOptions->color =!empty($value['color'])?$value['color']:'';
                    $productOptions->stock_items =!empty($value['stock_items'])?$value['stock_items']:'';
                    $productOptions->price =!empty($value['price'])?$value['price']:'';
                    $productOptions->discount_price =!empty($value['discount_price'])?$value['discount_price']:'';
                    $productOptions->image =$imageUrl;
                    $productOptions->defaults_check =!empty($value['defaults_check'])?$value['defaults_check']:'';;
                    $productOptions->save();
                }
            }

            $product->customFieldsValues()->createMany(getCustomFieldsValues($customFields, $request));
            if (isset($input['image']) && $input['image']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                if ($cacheUpload) {
                    $mediaItem = $cacheUpload->getMedia('image')->first();
                    $mediaItem->copy($product, 'image');
                }
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully', ['operator' => __('lang.product')]));

        return redirect(route('products.index'));
    }

    /**
     * Display the specified Product.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function show($id)
    {
        $this->productRepository->pushCriteria(new ProductsOfUserCriteria(auth()->id()));
        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function edit($id)
    {
        $this->productRepository->pushCriteria(new ProductsOfUserCriteria(auth()->id()));
        $product = $this->productRepository->findWithoutFail($id);
        if (empty($product)) {
            Flash::error(__('lang.not_found', ['operator' => __('lang.product')]));
            return redirect(route('products.index'));
        }
        $category = $this->categoryRepository->pluck('name', 'id');
        $brand = $this->brandRepository->pluck('name','id');
        if (auth()->user()->hasRole('admin')) {
            $store = $this->storeRepository->pluck('name', 'id');
        } else {
            $store = $this->storeRepository->myStores()->pluck('name', 'id');
        }
        $customFieldsValues = $product->customFieldsValues()->with('customField')->get();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->productRepository->model());
        $hasCustomField = in_array($this->productRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        $productOptions=[];
        if ($product){
            $productOptionsModel=$product->productOptions;
            if ($productOptionsModel){
                $productOptions=$productOptionsModel;
            }
        }

        return view('products.edit')->with('product', $product)->with('productOptions', $productOptions)->with("customFields", isset($html) ? $html : false)->with("store", $store)->with("category", $category)->with("brand",$brand);
    }

    /**
     * Update the specified Product in storage.
     *
     * @param int $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update($id, UpdateProductRequest $request)
    {
        $this->productRepository->pushCriteria(new ProductsOfUserCriteria(auth()->id()));
        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('Product not found');
            return redirect(route('products.index'));
        }
        $input = $request->all();
        $variationsInput = $request->all();

        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->productRepository->model());
        try {
            unset($input['productOptions']);
            $product = $this->productRepository->update($input, $id);
            if ($product){
                //Save And Update Product Options
                ProductOptions::where('product_id', $id)->delete();

                foreach ($variationsInput['productOptions'] as $key => $value) {

                        $oldImageUrl=isset($value['oldImageGet'])?$value['oldImageGet']:'';

                        $productImageVariations = $request->file('productOptions');
                        $productImage=!empty($productImageVariations[$key]['image'])?$productImageVariations[$key]['image']:'';
                        $imageUrl='';
                        if ($productImage) {
                            $imageName = $productImage->getClientOriginalName();
                            $directory = 'productImage/';
                            $imageUrl = $directory . $imageName;
                            Image::make($productImage)->save($imageUrl);
                        }else{
                            $imageUrl=$oldImageUrl;
                        }

                       $productOptions = new ProductOptions();
                       $productOptions->product_id =$product->id;
                       $productOptions->size = !empty($value['size'])?$value['size']:'';
                       $productOptions->color =!empty($value['color'])?$value['color']:'';
                       $productOptions->stock_items =!empty($value['stock_items'])?$value['stock_items']:'';
                       $productOptions->price =!empty($value['price'])?$value['price']:'';
                       $productOptions->discount_price =!empty($value['discount_price'])?$value['discount_price']:'';
                       $productOptions->image =$imageUrl;
                       $productOptions->defaults_check =!empty($value['defaults_check'])?$value['defaults_check']:'';;
                       $productOptions->save();
                }
            }
            if (isset($input['image']) && $input['image']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                if ($cacheUpload) {
                    $mediaItem = $cacheUpload->getMedia('image')->first();
                    $mediaItem->copy($product, 'image');
                }
            }
            foreach (getCustomFieldsValues($customFields, $request) as $value) {
                $product->customFieldsValues()
                    ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully', ['operator' => __('lang.product')]));

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (!env('APP_DEMO', false)) {
            $this->productRepository->pushCriteria(new ProductsOfUserCriteria(auth()->id()));
            $product = $this->productRepository->findWithoutFail($id);

            if (empty($product)) {
                Flash::error('Product not found');

                return redirect(route('products.index'));
            }

            $this->productRepository->delete($id);

            Flash::success(__('lang.deleted_successfully', ['operator' => __('lang.product')]));

        } else {
            Flash::warning('This is only demo app you can\'t change this section ');
        }
        return redirect(route('products.index'));
    }

    /**
     * Remove Media of Product
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $product = $this->productRepository->findWithoutFail($input['id']);
        try {
            if ($product->hasMedia($input['collection'])) {
                $product->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
