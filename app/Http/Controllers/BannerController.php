<?php

namespace App\Http\Controllers;

use App\DataTables\BannerDataTable;
use App\DataTables\CategoryDataTable;
use App\Models\Banner;
use App\Repositories\BannerRepository;
use App\Repositories\CategoryRepository;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Image;

class BannerController extends Controller
{
    private $bannerRepository;
    private $categoryRepository;

    public function __construct(BannerRepository $bannerRepo,CategoryRepository $categoryRepo)
    {
        parent::__construct();
        $this->bannerRepository = $bannerRepo;
        $this->categoryRepository = $categoryRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BannerDataTable $bannerDataTable)
    {
        return $bannerDataTable->render('banner.index');
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = $this->categoryRepository->pluck('name', 'id');

        $hasCustomField = in_array($this->bannerRepository->model(),setting('custom_field_models',[]));
        if($hasCustomField){
            $customFields = $this->bannerRepository->findByField('custom_field_model', $this->bannerRepository->model());
            $html = generateCustomField($customFields);
        }
        return view('banner.create')->with("customFields", isset($html) ? $html : false)->with("category", $category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        try {
            if ($input){
                //Save  Product Options
                foreach ($input['Banner'] as $key => $value) {
                    $bannaerImageVariotions = $request->file('Banner');
                    $bannerImage=!empty($bannaerImageVariotions[$key]['image'])?$bannaerImageVariotions[$key]['image']:'';
                    $imageUrl='';
                    if ($bannerImage) {
                        $imageName = $bannerImage->getClientOriginalName();
                        $directory = 'banners/';
                        $imageUrl = $directory . $imageName;
                        Image::make($bannerImage)->save($imageUrl);
                    }
                    $bannerOptions = new Banner();
                    $bannerOptions->name = !empty($value['name'])?$value['name']:'';
                    $bannerOptions->category =!empty($value['category'])?$value['category']:'';
                    $bannerOptions->status =!empty($value['status'])?$value['status']:'';
                    $bannerOptions->image =$imageUrl;
                    $bannerOptions->save();
                }
            }
        } catch (ValidationException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully', ['operator' => __('lang.banner')]));

        return redirect(route('banner.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = $this->bannerRepository->findWithoutFail($id);

        $category = $this->categoryRepository->pluck('name', 'id');

        if (empty($banner)) {
            Flash::error(__('lang.not_found',['operator' => __('lang.banner')]));

            return redirect(route('categories.index'));
        }

        return view('banner.edit')->with('category', $category)->with('banner', $banner);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banner = $this->bannerRepository->findWithoutFail($id);

        if (empty($banner)) {
            Flash::error('Category not found');
            return redirect(route('categories.index'));
        }
        $input = $request->all();
        try {
            if(isset($input['image']) && $input['image']){
                $bannaerImageVariotions = $request->file();
                $bannerImage=!empty($bannaerImageVariotions['image'])?$bannaerImageVariotions['image']:'';
                $imageUrl='';
                if ($bannerImage) {
                    $imageName = $bannerImage->getClientOriginalName();
                    $directory = 'banners/';
                    $imageUrl = $directory . $imageName;
                    Image::make($bannerImage)->save($imageUrl);
                }
                $input['image']=$imageUrl;
            }
            $banner = $this->bannerRepository->update($input, $id);
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully',['operator' => __('lang.banner')]));

        return redirect(route('banner.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = $this->bannerRepository->findWithoutFail($id);

        if (empty($banner)) {
            Flash::error('Category not found');

            return redirect(route('banner.index'));
        }

        $this->bannerRepository->delete($id);

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.banner')]));

        return redirect(route('banner.index'));
    }
}
