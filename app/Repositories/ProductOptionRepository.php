<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\ProductOptions;
use InfyOm\Generator\Common\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class ProductRepository
 * @package App\Repositories
 * @version August 29, 2019, 9:38 pm UTC
 *
 * @method Product findWithoutFail($id, $columns = ['*'])
 * @method Product find($id, $columns = ['*'])
 * @method Product first($columns = ['*'])
 */
class ProductOptionRepository extends BaseRepository implements CacheableInterface
{

    use CacheableRepository;
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'size',
        'color',
        'stock_items',
        'price',
        'discount_price',
        'defaults_check',
        'image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductOptions::class;
    }

    /**
     * get my products
     **/
    public function myProducts()
    {
        return ProductOptions::join("user_stores", "user_stores.store_id", "=", "products.store_id")
            ->where('user_stores.user_id', auth()->id())->get();
    }

    public function groupedByStores()
    {
        $products = [];
        foreach ($this->all() as $model) {
            $products[$model->store->name][$model->id] = $model->name;
        }
        return $products;
    }
}
