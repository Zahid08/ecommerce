<?php
$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/new_laravel_project/public/';
?>
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">
<!-- Name Field -->
<div class="form-group row ">
  {!! Form::label('name', trans("lang.banner_name"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::text('name', null,  ['class' => 'form-control','placeholder'=>  trans("lang.banner_name_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.category_name_help") }}
    </div>
  </div>
</div>

    <!-- Category Id Field -->
    <div class="form-group row ">
        {!! Form::label('category_id', trans("lang.product_category_id"),['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::select('category_id', $category, null, ['class' => 'select2 form-control']) !!}
            <div class="form-text text-muted">{{ trans("lang.product_category_id_help") }}</div>
        </div>
    </div>

    <div class="form-group row ">
        <label for="status" class="col-3 control-label text-right">Image</label>
        <div class="col-9">
            <input style="overflow: hidden"  type="file" name="image" placeholder="Enter image" class="form-control"/>
            <input  type="hidden" name="oldImageGet" class="form-control"  value="{{$banner->image}}" />
            <img style="width:50px;height:50px;" src="{{$root.$banner->image}}">
        </div>
    </div>
    
    <div class="form-group row ">
        <label for="status" class="col-3 control-label text-right">Status</label>
        <div class="col-9">
        <select class="form-control"  name="status">
            <option value="1">Active</option>
            <option value="0">Inactive</option>
        </select>
        </div>
    </div>
<!-- Submit Field -->
<div class="form-group col-12 text-right">
  <button type="submit" class="btn btn-{{setting('theme_color')}}" ><i class="fa fa-save"></i> {{trans('lang.save')}} {{trans('lang.banner')}}</button>
  <a href="{!! route('banner.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.cancel')}}</a>
</div>
</div>