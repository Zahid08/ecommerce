<div class='btn-group btn-group-sm'>
  @can('banner.edit')
  <a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.banner_edit')}}" href="{{ route('banner.edit', $id) }}" class='btn btn-link'>
    <i class="fa fa-edit"></i>
  </a>
  @endcan

  @can('categories.destroy')
{!! Form::open(['route' => ['banner.destroy', $id], 'method' => 'delete']) !!}
  {!! Form::button('<i class="fa fa-trash"></i>', [
  'type' => 'submit',
  'class' => 'btn btn-link text-danger',
  'onclick' => "return confirm('Are you sure?')"
  ]) !!}
{!! Form::close() !!}
  @endcan
</div>
