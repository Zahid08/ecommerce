<?php
/**
 * Created by PhpStorm.
 * User: Zahid
 * Date: 08-Dec-20
 * Time: 9:16 AM
 */
?>

<table class="table table-bordered" id="dynamicBannerOptions">
    <tr>
        <th>Image Title</th>
        <th>Image</th>
        <th>Category</th>
        <th>Status</th>
        <th>Action</th>
    </tr>
        <tr>
            <td><input type="text" name="Banner[0][name]" placeholder="Enter title" class="form-control" required /></td>
            <td><input style="overflow: hidden"  type="file" name="Banner[0][image]" placeholder="Enter image" class="form-control" required/></td>
            <td>
                    <select class="form-control"  name="Banner[0][category]" id="categoryBlock" required>
                       @foreach ($category as $key=>$categoryItem)
                            <option value="{{$key}}">{{$categoryItem}}</option>
                        @endforeach
                    </select>
            </td>
            <td>
                <select class="form-control"  name="Banner[0][status]">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </td>
            <td>
                <button type="button" class="btn btn-danger remove-tr">X</button>
            </td>
        </tr>
</table>

<button type="button" name="add" id="add" class="btn btn-success">Add more option</button>

@prepend('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            var index = $('#dynamicBannerOptions tbody tr').length;
            var category = $('#categoryBlock').html();

            $("#add").click(function(){
                $("#dynamicBannerOptions").append('<tr>' +
                    '<td><input type="text" name="Banner['+index+'][name]" placeholder="Enter title" class="form-control" /></td>' +
                    '<td><input style="overflow: hidden"  type="file" name="Banner['+index+'][image]" placeholder="Enter image" class="form-control" /></td>' +
                    '<td><select class="form-control"  name="Banner['+index+'][category]">'+category+'</select></td>' +
                    '<td>  <select class="form-control"  name="Banner['+index+'][status]">\n' +
                    '<option value="1">Active</option>\n' +
                    '<option value="0">Inactive</option>\n' +
                    '</select></td>' +
                    '<td><button type="button" class="btn btn-danger remove-tr">X</button></td></tr>');
                index++;
            });
        });

        $(document).on('click', '.remove-tr', function(){
            $(this).parents('tr').remove();
        });
    </script>
@endprepend