<div class='btn-group btn-group-sm'>
  @can('products.show')
  <a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.view_details')}}" href="{{ route('products.show', $id) }}" class='btn btn-link'>
    <i class="fa fa-eye"></i>
  </a>
  @endcan

  @can('products.edit')
  <a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.product_edit')}}" href="{{ route('products.edit', $id) }}" class='btn btn-link'>
    <i class="fa fa-edit"></i>
  </a>
  @endcan

  @can('products.destroy')
{!! Form::open(['route' => ['products.destroy', $id], 'method' => 'delete']) !!}
  {!! Form::button('<i class="fa fa-trash"></i>', [
  'type' => 'submit',
  'class' => 'btn btn-link text-danger',
  'onclick' => "return confirm('Are you sure?')"
  ]) !!}
{!! Form::close() !!}
  @endcan

    @can('products.edit')
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">View Options</button>
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-body">
            <h4 style="border-bottom:1px solid gray;margin-bottom:14px;" class="modal-title">View Product Options</h4>
           <?php
            $productOptions=\App\Models\ProductOptions::where('product_id',$id)->get();
            ?>
             <table class="table table-bordered" id="dynamicProductOptions">
               <tr>
                 <th>Size</th>
                 <th>Color</th>
                 <th>Stock Items</th>
                 <th>Price</th>
                 <th>Discount Price</th>
               </tr>
               @if (isset($productOptions) && !$productOptions->isEmpty())
                 @foreach ($productOptions as $key=>$productOptionsItem)
                 <tr>
                   <td>{{$productOptionsItem->size}}</td>
                   <td>{{$productOptionsItem->color}}</td>
                   <td>{{$productOptionsItem->stock_items}}</td>
                   <td>{{$productOptionsItem->price}}</td>
                   <td>{{$productOptionsItem->discount_price}}</td>
                 </tr>
                 @endforeach
               @else
                 <tr>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td></td>
                 </tr>
               @endif
             </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
    @endcan
</div>
