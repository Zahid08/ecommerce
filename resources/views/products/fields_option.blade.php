<?php
/**
 * Created by PhpStorm.
 * User: Zahid
 * Date: 08-Dec-20
 * Time: 9:16 AM
 */

?>

<table class="table table-bordered" id="dynamicProductOptions">
    <tr>
        <th>Size</th>
        <th>Color</th>
        <th>Stock Items</th>
        <th>Price</th>
        <th>Discount Price</th>
        <th>Image</th>
        <th>Default</th>
        <th>Action</th>
    </tr>
    @if (isset($productOptions) && !$productOptions->isEmpty())
        @foreach ($productOptions as $key=>$productOptionsItem)
            <?php
            $root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/new_laravel_project';
            ?>
            <tr>
                <td style="display: none"><input type="hidden" name="productOptions[{{$key}}][product_id]" placeholder="Enter product size" class="form-control" value="{{$productOptionsItem->product_id}}" /></td>
                <td><input type="text" name="productOptions[{{$key}}][size]" placeholder="Enter product size" class="form-control" value="{{$productOptionsItem->size}}" /></td>
                <td><input type="text" name="productOptions[{{$key}}][color]" placeholder="Enter color" class="form-control"  value="{{$productOptionsItem->color}}" /></td>
                <td><input type="text" name="productOptions[{{$key}}][stock_items]" placeholder="Enter stock item" class="form-control" value="{{$productOptionsItem->stock_items}}" /></td>
                <td><input type="text" name="productOptions[{{$key}}][price]" placeholder="Enter price" class="form-control"  value="{{$productOptionsItem->price}}"  /></td>
                <td><input type="text" name="productOptions[{{$key}}][discount_price]" placeholder="Enter discount price" class="form-control"  value="{{$productOptionsItem->discount_price}}" /></td>
                <td style="display:none;"><input  type="text" name="productOptions[{{$key}}][oldImageGet]" class="form-control"  value="{{$productOptionsItem->image}}" /></td>
                <td><span><input style="overflow: hidden" type="file" name="productOptions[{{$key}}][image]" class="form-control" /> <img style="width: 58px;" src="{{$root.'/public/'.$productOptionsItem->image}}"></span></td>
                <td><input type="checkbox" data-index="0" id="defualtCheckOptions{{$key}}" name="productOptions[{{$key}}][defaults_check]" class="form-control defualtCheckOptionsClass" value="" <?php
                        if ($productOptionsItem->defaults_check==1){
                            echo "checked";
                        }
                        ?> /></td>
                <td><button type="button" data-product-id="{{$productOptionsItem->product_id}}" class="btn btn-danger remove-tr">X</button></td></td>
            </tr>
        @endforeach
    @else

        <tr>
            <td><input type="text" name="productOptions[0][size]" placeholder="Enter product size" class="form-control" required /></td>
            <td><input type="text" name="productOptions[0][color]" placeholder="Enter color" class="form-control" required/></td>
            <td><input type="text" name="productOptions[0][stock_items]" placeholder="Enter stock item" class="form-control" required/></td>
            <td><input type="text" name="productOptions[0][price]" placeholder="Enter price" class="form-control" required/></td>
            <td><input type="text" name="productOptions[0][discount_price]" placeholder="Enter discount price" class="form-control" required/></td>
            <td><span><input style="overflow: hidden" type="file" name="productOptions[0][image]" class="form-control" /></span></td>
            <td><input type="checkbox" data-index="0" id="defualtCheckOptions0" name="productOptions[0][defaults_check]" class="form-control defualtCheckOptionsClass" value="" /></td>
            <td><button type="button" class="btn btn-danger remove-tr">X</button></td></td>
        </tr>
    @endif
</table>

<button type="button" name="add" id="add" class="btn btn-success">Add more option</button>

@prepend('scripts')
    <script type="text/javascript">
    $( document ).ready(function() {
        var index = $('#dynamicProductOptions tbody tr').length;
        $("#add").click(function(){
            $("#dynamicProductOptions").append('<tr>' +
                '<td><input type="text" name="productOptions['+index+'][size]" placeholder="Enter product size" class="form-control" /></td>' +
                '<td><input type="text" name="productOptions['+index+'][color]" placeholder="Enter color" class="form-control" /></td>' +
                '<td><input type="text" name="productOptions['+index+'][stock_items]" placeholder="Enter stock item" class="form-control" /></td>' +
                '<td><input type="text" name="productOptions['+index+'][price]" placeholder="Enter price" class="form-control" /></td>' +
                '<td><input type="text" name="productOptions['+index+'][discount_price]" placeholder="Enter discount price" class="form-control" /></td>' +
                '<td><span><input type="file" style="overflow: hidden" name="productOptions['+index+'][image]" class="form-control" /></span></td>' +
                '<td><input type="checkbox" data-index="'+index+'" id="defualtCheckOptions'+index+'" name="productOptions['+index+'][defaults_check]" placeholder="Enter discount price" class="form-control defualtCheckOptionsClass"  value="" /></td>' +
                '<td><button type="button" class="btn btn-danger remove-tr">X</button></td></tr>');
            index++;
        });
    });

    $(document).on('click', '.defualtCheckOptionsClass', function(){
       var Index=$(this).data('index');
        if($("#defualtCheckOptions"+Index+"").prop('checked') == true){
           $(this).val(1);
        }else {
            $(this).val(0);
        }
    });

    $(document).on('click', '.remove-tr', function(){
        $(this).parents('tr').remove();
    });
</script>
@endprepend